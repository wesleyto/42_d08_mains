#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "ft.h"
#include "nft.c"

int main(void)
{
	int		a;
	int		b;
	int		eql;
	int		len;

	a = 5;
	b = 3;
	ft_putchar('H');
	ft_putstr("ello\n");
	eql = ft_strcmp("Something", "Hello");
	len = ft_strlen("Hello");
	ft_swap(&a, &b);
	return (0);
}