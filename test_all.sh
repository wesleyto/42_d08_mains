LAST_EX=4

for num in $(seq -f "%02g" 0 $LAST_EX)
	do
		if [ -e ex$num/ft*.c ]
		then
			norminette -R CheckForbiddenSourceHeader ex$num/ft*.c
		fi
		if [ -e ex$num/ft*.h ]
		then
			norminette -R CheckForbiddenSourceHeader ex$num/ft*.h
		fi
	done


for num in $(seq -f "%02g" 0 $LAST_EX)
	do
		gcc -Wall -Wextra -Werror -o ex$num/main ex$num/main.c
	done

for num in $(seq -f "%02g" 0 $LAST_EX)
	do
		echo ===========================
		echo ===== Testing Ex$num =====
		echo ===========================
		./ex$num/main
		echo
	done