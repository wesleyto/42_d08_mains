/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/17 14:58:11 by wto               #+#    #+#             */
/*   Updated: 2017/08/17 15:00:57 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_point.h"

void 	set_point(t_point *point)
{
	point->x = 42;
	point->y = 21;
}

int		main(void)
{
	t_point	point;

	set_point(&point);
	return (0);
}
