/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/17 12:14:05 by wto               #+#    #+#             */
/*   Updated: 2017/08/17 12:19:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_abs.h"
#include <stdio.h>

int main(void)
{
	int cases[] = {-2147483648, -2147483647, -1, 0, 1, 2147483647};
	printf("Ints: %d tests\n", (int)(sizeof(cases) / sizeof(int)));
	for (int i = 0; i < (int)sizeof(cases) / (int)sizeof(int); i++)
	{
		int test = cases[i];
		int result = ABS(cases[i]);
		printf("\t%s || %-21d -> %-21d\n", (test < 0 ? test * -1 : test) == result ? "Success" : "Failure", test, result);
	}
	long cases_l[] = {
		-9223372036854775807L, 
		-2147483648, 
		-1, 
		0, 
		1, 
		2147483647, 
		2147483648L,
		9223372036854775807L};
	printf("Longs: %d tests\n", (int)(sizeof(cases_l) / sizeof(long)));
	for (int i = 0; i < (int)sizeof(cases_l) / (int)sizeof(long); i++)
	{
		long test = cases_l[i];
		long result = ABS(cases_l[i]);
		printf("\t%s || %-21ld -> %-21ld\n", (test < 0 ? test * -1 : test) == result ? "Success" : "Failure", test, result);
	}
	return (0);
}
